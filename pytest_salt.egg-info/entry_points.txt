[pytest11]
salt = pytestsalt
salt.config = pytestsalt.fixtures.config
salt.daemons = pytestsalt.fixtures.daemons
salt.dirs = pytestsalt.fixtures.dirs
salt.log = pytestsalt.fixtures.log
salt.ports = pytestsalt.fixtures.ports
salt.stats = pytestsalt.fixtures.stats

[salt.loader]
engines_dirs = pytestsalt.salt.loader:engines_dirs
log_handlers_dirs = pytestsalt.salt.loader:log_handlers_dirs

