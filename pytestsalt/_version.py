
# This file was generated by 'versioneer.py' (0.18) from
# revision-control system data, or from the parent directory name of an
# unpacked source archive. Distribution tarballs contain a pre-generated copy
# of this file.

from __future__ import absolute_import
import json

version_json = '''
{
 "date": "2019-06-12T13:32:25+0100",
 "dirty": false,
 "error": null,
 "full-revisionid": "5d8883b10459d2ec7fe65566a1d773b936bec649",
 "version": "2019.6.13"
}
'''  # END VERSION_JSON


def get_versions():
    return json.loads(version_json)
